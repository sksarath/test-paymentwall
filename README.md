## Getting Started

  * Clone the Repo
  * cd into repo_name
  * follow the Build Setup Process
  
> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build

```